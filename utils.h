#ifndef UTILS_H_e6b0cb11_6bb2_4861_b359_5172e6aed0db
#define UTILS_H_e6b0cb11_6bb2_4861_b359_5172e6aed0db

#include <QString>
#include <QDir>

namespace ADPT
{
    QString slugify(const QString& str);
    void makeAbsolutePath_local(const QDir& rootDir, QString& path);
}

#endif // UTILS_H
