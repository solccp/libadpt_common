#ifndef VARIANTQT_H
#define VARIANTQT_H

#include <iostream>
#include <exception>
#include <stdexcept>
#include <Variant/Variant.h>
#include <array>

#include <QMap>
#include <QList>
#include <QString>
#include <QStringList>
#include <QVariant>

#include <memory>

class KeywordNotFound : public std::runtime_error
{
public:
    KeywordNotFound(std::string node_name): std::runtime_error(node_name){}
};

class KeywordParseError : public std::runtime_error
{
public:
    KeywordParseError(std::string node_name): std::runtime_error(node_name){}
};

class SchemaError : public std::runtime_error
{
public:
    SchemaError(): std::runtime_error(""){}
};


#define COMMA ,

#define GET_VARIANT_LIST(nodelhs, node, node_index, node_type) \
    try \
    { \
        libvariant::VariantConvertor<node_type>::fromVariant(node.At(node_index), nodelhs); \
    } \
    catch(KeywordNotFound & e) \
    { \
        throw KeywordNotFound(QString("[%1]").arg(node_index).toStdString() + std::string("/") + e.what()); \
    } \
    catch(KeywordParseError & e) \
    { \
        throw KeywordParseError(QString("[%1]").arg(node_index).toStdString() + std::string("/") + e.what()); \
    } \
    catch(std::exception &e) \
    { \
        KeywordParseError error(QString("[%1]").arg(node_index).toStdString()); \
        throw error; \
    } ;


#define GET_VARIANT(nodelhs, node, node_name, node_type) \
    if (node.Contains(node_name)) \
    { \
        try \
        { \
           libvariant::VariantConvertor<node_type>::fromVariant(node[node_name], nodelhs); \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }\
    } \
    else \
    { \
        throw KeywordNotFound(std::string(node_name)); \
    } \

#define GET_VARIANT_OPT(nodelhs, node, node_name, node_type, hasValue) \
    { \
        hasValue = false; \
        try \
        { \
            if (node.Contains(node_name)) \
            { \
               libvariant::VariantConvertor<node_type>::fromVariant(node[node_name], nodelhs); \
               hasValue = true; \
            } \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }; \
    }

#define GET_VARIANT_1(nodelhs, node, node_name, node_type, para) \
    if (node.Contains(node_name)) \
    { \
        try \
        { \
           libvariant::VariantConvertor<node_type>::fromVariant_1(node[node_name], nodelhs, para); \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        } \
    } \
    else \
    { \
        throw KeywordNotFound(std::string(node_name)); \
    } \


#define GET_VARIANT_1_OPT(nodelhs, node, node_name, node_type, hasValue, para) \
    { \
        hasValue = false; \
        try \
        { \
            if (node.Contains(node_name)) \
            { \
               libvariant::VariantConvertor<node_type>::fromVariant_1(node[node_name], nodelhs, para); \
               hasValue = true; \
            } \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            throw KeywordParseError(node_name + std::string("/") + e.what()); \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }; \
    }

//ptr
#define GET_VARIANT_PTR(nodelhs, node, node_name, node_type) \
    if (node.Contains(node_name)) \
    { \
        try \
        { \
           nodelhs = std::make_shared<node_type>(); \
           libvariant::VariantConvertor<node_type>::fromVariant(node[node_name], *nodelhs); \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            throw KeywordParseError(node_name + std::string("/") + e.what()); \
        } \
        catch(std::exception &e) \
        { \
            throw KeywordParseError(node_name); \
        } \
    } \
    else \
    { \
        throw KeywordNotFound(std::string(node_name)); \
    }

#define GET_VARIANT_PTR_OPT(nodelhs, node, node_name, node_type, hasValue) \
    { \
        hasValue = false; \
        try \
        { \
            if (node.Contains(node_name)) \
            { \
               nodelhs = std::make_shared<node_type>(); \
               libvariant::VariantConvertor<node_type>::fromVariant(node[node_name], *nodelhs); \
               hasValue = true; \
            } \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }; \
    }

#define GET_VARIANT_PTR_1(nodelhs, node, node_name, node_type, para) \
    if (node.Contains(node_name)) \
    { \
        try \
        { \
            nodelhs = std::make_shared<node_type>(); \
            libvariant::VariantConvertor<node_type>::fromVariant_1(node[node_name], *nodelhs, para); \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }\
    } \
    else \
    { \
        throw KeywordNotFound(std::string(node_name)); \
    }

#define GET_VARIANT_PTR_1_OPT(nodelhs, node, node_name, node_type, hasValue, para) \
    { \
        hasValue = false; \
        try \
        { \
            if (node.Contains(node_name)) \
            { \
               nodelhs = std::make_shared<node_type>(); \
               libvariant::VariantConvertor<node_type>::fromVariant_1(node[node_name], *nodelhs, para); \
               hasValue = true; \
            } \
        } \
        catch(KeywordNotFound & e) \
        { \
            throw KeywordNotFound(node_name + std::string("/") + e.what()); \
        } \
        catch(KeywordParseError & e) \
        { \
            KeywordParseError error(node_name + std::string("/") + e.what()); \
            throw error; \
        } \
        catch(std::exception &e) \
        { \
            KeywordParseError error(node_name); \
            throw error; \
        }; \
    }


typedef std::array<int, 3> iVector3;
typedef std::array<double, 3> dVector3;
typedef std::array<std::array<int, 3>, 3> iMatrix3x3;
typedef std::array<std::array<double, 3>, 3> dMatrix3x3;

namespace libvariant
{
    template<typename T>
    struct VariantConvertor
    {
        static libvariant::Variant toVariant(const T &rhs)
        {
            return libvariant::Variant(rhs);
        }

        static void fromVariant(const libvariant::Variant &variant, T &rhs)
        {
            rhs = libvariant::VariantCaster<T>::Cast(variant);
        }

        template<typename Args_1>
        static void fromVariant_1(const libvariant::Variant &variant, T &rhs, Args_1&)
        {
            rhs = libvariant::VariantCaster<T>::Cast(variant);
        }

    };


    template<typename T>
    struct VariantConvertor<QPair<T, T> >
    {
        static Variant toVariant(const QPair<T, T> &rhs)
        {
            Variant var;
            var.Append(VariantConvertor<T>::toVariant(rhs.first));
            var.Append(VariantConvertor<T>::toVariant(rhs.second));
            return var;
        }
        static void fromVariant(const Variant &var, QPair<T, T> &rhs)
        {
            VariantConvertor<T>::fromVariant(var[0], rhs.first);
            VariantConvertor<T>::fromVariant(var[1], rhs.second);
        }
    };

    template<typename T, typename V>
    struct VariantConvertor<QPair<T, V> >
    {
        static Variant toVariant(const QPair<T, V> &rhs)
        {
            Variant var;
            var.Append(VariantConvertor<T>::toVariant(rhs.first));
            var.Append(VariantConvertor<V>::toVariant(rhs.second));
            return var;
        }
        static void fromVariant(const Variant &var, QPair<T, V> &rhs)
        {
            VariantConvertor<T>::fromVariant(var[0], rhs.first);
            VariantConvertor<V>::fromVariant(var[1], rhs.second);
        }
    };


    template<>
    struct VariantConvertor<iVector3>
    {
        static Variant toVariant(const iVector3 &rhs)
        {
            Variant var;
            var.Append(rhs[0]);
            var.Append(rhs[1]);
            var.Append(rhs[2]);
            return var;
        }
        static void fromVariant(const Variant &var, iVector3 &rhs)
        {
            rhs[0] = var[0].AsInt();
            rhs[1] = var[1].AsInt();
            rhs[2] = var[2].AsInt();
        }
    };

    template<>
    struct VariantConvertor<dVector3>
    {
        static Variant toVariant(const dVector3 &rhs)
        {
            Variant var;
            var.Append(rhs[0]);
            var.Append(rhs[1]);
            var.Append(rhs[2]);
            return var;
        }
        static void fromVariant(const Variant &var, dVector3 &rhs)
        {
            rhs[0] = var[0].AsDouble();
            rhs[1] = var[1].AsDouble();
            rhs[2] = var[2].AsDouble();
        }
    };

    template<>
    struct VariantConvertor<dMatrix3x3>
    {
        static Variant toVariant(const dMatrix3x3 &rhs)
        {
            Variant var;
            var.Append(VariantConvertor<dVector3>::toVariant(rhs[0]));
            var.Append(VariantConvertor<dVector3>::toVariant(rhs[1]));
            var.Append(VariantConvertor<dVector3>::toVariant(rhs[2]));
            return var;
        }
        static void fromVariant(const Variant &var, dMatrix3x3 &rhs)
        {
            VariantConvertor<dVector3>::fromVariant(var[0], rhs[0]);
            VariantConvertor<dVector3>::fromVariant(var[1], rhs[1]);
            VariantConvertor<dVector3>::fromVariant(var[2], rhs[2]);
        }
    };



    template<>
    struct VariantConvertor<QVariant>
    {
        static libvariant::Variant toVariant(const QVariant &rhs)
        {
            return libvariant::Variant(rhs.toString().toStdString());
        }

        static void fromVariant(const libvariant::Variant &variant, QVariant &rhs)
        {
            rhs = QString::fromStdString(libvariant::VariantCaster<std::string>::Cast(variant)).trimmed();
        }
    };






    template<typename T>
    struct VariantConvertor<QList<T> >
    {
        static libvariant::Variant toVariant(const QList<T> &rhs)
        {
            std::vector<libvariant::Variant> list;
            for (int i = 0; i < rhs.size(); ++i)
            {
                list.push_back(VariantConvertor<T>::toVariant(rhs[i]));
            }
            return list;
        }
        static void fromVariant(const libvariant::Variant &var, QList<T> &rhs)
        {
            for (unsigned i = 0; i < var.Size(); ++i)
            {
                try
                {
                    T value;
                    VariantConvertor<T>::fromVariant(var[i], value);
                    rhs.append(value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString());
                }
            }
        }

        template<typename Args>
        static void fromVariant_1(const libvariant::Variant &var, QList<T> &rhs, Args& args)
        {
            for (unsigned i = 0; i < var.Size(); ++i)
            {
                try
                {
                    T value;
                    VariantConvertor<T>::fromVariant_1(var[i], value, args);
                    rhs.append(value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString());
                }
            }
        }
    };

    template<typename T>
    struct VariantConvertor<QMap<QString, T> >
    {
        static Variant toVariant(const QMap<QString, T> &rhs)
        {
            std::map<std::string, libvariant::Variant> map;
            QMapIterator<QString, T> it(rhs);
            while (it.hasNext())
            {
                it.next();
                libvariant::Variant node = VariantConvertor<T>::toVariant(it.value());
                map[it.key().toStdString()] = node;
            }
            return map;
        }
        static void fromVariant(const libvariant::Variant &var, QMap<QString, T> &rhs)
        {
            if (var.IsNull())
            {
                return;
            }
            auto map = var.AsMap();
            for (auto i = map.begin(); i != map.end(); ++i)
            {
                try
                {
                    T value;
                    VariantConvertor<T>::fromVariant(i->second, value);
                    rhs.insert(QString::fromStdString(i->first).trimmed(), value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(i->first + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(i->first + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(i->first);;
                }
            }
        }

        template<typename Args>
        static void fromVariant_1(const libvariant::Variant &var, QMap<QString, T> &rhs, Args& args)
        {
            if (var.IsNull())
            {
                return;
            }
            auto map = var.AsMap();
            for (auto i = map.begin(); i != map.end(); ++i)
            {
                try
                {
                    T value;
                    VariantConvertor<T>::fromVariant_1(i->second, value, args);
                    rhs.insert(QString::fromStdString(i->first).trimmed(), value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(i->first + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(i->first + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(i->first);
                }
            }
        }

    };



    template<typename T>
    struct VariantConvertor<QVector<T> >
    {
        static libvariant::Variant toVariant(const QVector<T> &rhs)
        {
            std::vector<libvariant::Variant> list;
            for (int i = 0; i < rhs.size(); ++i)
            {
                list.push_back(VariantConvertor<T>::toVariant(rhs[i]));
            }
            return list;
        }
        static void fromVariant(const libvariant::Variant &var, QVector<T> &rhs)
        {
            for (unsigned i = 0; i < var.Size(); ++i)
            {
                try
                {
                    T value;
                    VariantConvertor<T>::fromVariant(var[i], value);
                    rhs.append(value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString());
                }
            }
        }
    };



    template<>
    struct VariantConvertor<QString>
    {
        static libvariant::Variant toVariant(const QString &rhs)
        {
            return libvariant::Variant(rhs.toStdString());
        }
        static void fromVariant(const libvariant::Variant &variant, QString &rhs)
        {
            rhs = QString::fromStdString(libvariant::VariantCaster<std::string>::Cast(variant)).trimmed();
        }
    };

    template<>
    struct VariantConvertor<QStringList>
    {
        static libvariant::Variant toVariant(const QStringList &rhs)
        {
            std::vector<libvariant::Variant> list;
            for (int i = 0; i < rhs.size(); ++i)
            {
                list.push_back(VariantConvertor<QString>::toVariant(rhs[i]));
            }
            return list;
        }
        static void fromVariant(const libvariant::Variant &var, QStringList &rhs)
        {
            for (unsigned i = 0; i < var.Size(); ++i)
            {
                try
                {
                    QString str;
                    VariantConvertor<QString>::fromVariant(var[i], str);
                    rhs.append(str);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString());
                }
            }
        }
    };


    template<>
    struct VariantConvertor<QVariantList>
    {
        static libvariant::Variant toVariant(const QVariantList &rhs)
        {
            std::vector<libvariant::Variant> list;
            for (int i = 0; i < rhs.size(); ++i)
            {
                list.push_back(VariantConvertor<QVariant>::toVariant(rhs[i]));
            }
            return list;
        }
        static void fromVariant(const libvariant::Variant &var, QVariantList &rhs)
        {
            for (unsigned i = 0; i < var.Size(); ++i)
            {
                try
                {
                    QVariant value;
                    VariantConvertor<QVariant>::fromVariant(var[i], value);
                    rhs.append(value);
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString() + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(QString("[%1]").arg(i).toStdString());
                }
            }
        }


    };

    template<>
    struct VariantConvertor<QVariantMap>
    {
        static Variant toVariant(const QVariantMap &rhs)
        {
            std::map<std::string, libvariant::Variant> map;
            QMapIterator<QString, QVariant> it(rhs);
            while (it.hasNext())
            {
                it.next();
                libvariant::Variant node;
                if (it.value().canConvert<QVariantMap>())
                {
                    QVariantMap value = it.value().toMap();
                    node = VariantConvertor<QVariantMap>::toVariant(value);
                }
                else if (it.value().canConvert<QVariantList>())
                {
                    QVariantList value = it.value().toList();
                    node = VariantConvertor<QVariantList>::toVariant(value);
                }
                else
                {
                    node = VariantConvertor<QVariant>::toVariant(it.value());
                }

                map[it.key().toStdString()] = node;
            }
            return map;
        }
        static void fromVariant(const libvariant::Variant &var, QVariantMap &rhs)
        {
            if (var.IsNull())
            {
                return;
            }
            auto map = var.AsMap();
            for (auto i = map.begin(); i != map.end(); ++i)
            {
                try
                {
                    if (i->second.IsMap())
                    {
                        QVariantMap value;
                        VariantConvertor<QVariantMap>::fromVariant(i->second.AsMap(), value);
                        rhs.insert(QString::fromStdString(i->first).trimmed(), value);
                    }
                    else if (i->second.IsList())
                    {
                        QVariantList value;
                        VariantConvertor<QVariantList>::fromVariant(i->second.AsList(), value);
                        rhs.insert(QString::fromStdString(i->first).trimmed(), value);
                    }
                    else
                    {
                        QVariant value;
                        VariantConvertor<QVariant>::fromVariant(i->second, value);
                        rhs.insert(QString::fromStdString(i->first).trimmed(), value);
                    }
                }
                catch(KeywordNotFound & e)
                {
                    throw KeywordNotFound(i->first + std::string("/") + e.what());
                }
                catch(KeywordParseError & e)
                {
                    throw KeywordParseError(i->first + std::string("/") + e.what());
                }
                catch(std::exception &e)
                {
                    throw KeywordParseError(i->first);;
                }
            }
        }
    };


}

#endif // VARIANTQT_H
