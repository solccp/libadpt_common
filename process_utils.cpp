
#include "process_utils.h"

#include <QProcessEnvironment>
#include <QProcess>
#include <QDir>
#include <QFileInfo>

namespace ADPT
{

    QString which(const QString &exec)
    {
        if (exec.startsWith("~") ||QDir::isAbsolutePath(exec) )
        {

            QString final_exec = exec;
            final_exec.replace("~", QDir::homePath());
            QFileInfo info = QFileInfo(final_exec);
            if (info.exists() && info.isExecutable())
            {
                return info.absoluteFilePath();
            }
        }

        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        QStringList pathes = env.value("PATH").split(":") ;
        for(int i=0; i<pathes.size();++i)
        {
            QDir path = QDir(pathes[i]);
            QFileInfo info = QFileInfo(path.absoluteFilePath(exec));
            if (info.exists() && info.isExecutable())
            {
                return info.absoluteFilePath();
            }
        }

        QFileInfo info = QFileInfo(exec);
        return info.absoluteFilePath();
    }

    QString expandpath(const QString &file)
    {
        QString res = file;
        if (file.startsWith('~'))
        {
            res = QDir::homePath() + file.mid(1);
        }
        QFileInfo info = QFileInfo(res);
        return info.absoluteFilePath();
    }

    bool ExecuteCommandStream(const QString &workingDir, const QString &executable, const QStringList &arguments, QByteArray &output, const QByteArray &infile, int timeout)
    {
        QProcess m_process;

        m_process.setWorkingDirectory(workingDir);
        m_process.start(executable, arguments);
        m_process.write(infile);
        m_process.closeWriteChannel();
        int timeout_in_msec = timeout*1000;
        if (timeout_in_msec < 0)
            timeout_in_msec = -1;
        m_process.waitForFinished(timeout_in_msec);

        if (m_process.exitCode()!=0)
        {
            return false;
        }

        output = m_process.readAll();
        return true;
    }

    bool ExecuteCommand(const QString &workingDir, const QString &executable, const QStringList &arguments, QByteArray &output, const QString &infile, int timeout)
    {

        QProcess m_process;
        if (!infile.isNull() && !infile.isEmpty())
        {
            m_process.setStandardInputFile(infile);
        }
        m_process.setWorkingDirectory(workingDir);
        m_process.start(executable, arguments);
        if (infile.isEmpty())
        {
            m_process.closeWriteChannel();
        }
        int timeout_in_msec = timeout*1000;
        if (timeout_in_msec < 0)
            timeout_in_msec = -1;
        m_process.waitForFinished(timeout_in_msec);

        if (m_process.exitCode()!=0)
        {
            return false;
        }

        output = m_process.readAll();
        return true;
    }


}
