#ifndef ATOMICPROPERTIES_H_4025ae3e_88af_46c1_bd11_eb4713de343f
#define ATOMICPROPERTIES_H_4025ae3e_88af_46c1_bd11_eb4713de343f

#include <QString>
#include <QMap>
#include <QVector>

namespace ADPT
{

class AtomicProperties
{
public:
    static const AtomicProperties ERROR_ATOM;
    static const AtomicProperties H;
    static const AtomicProperties He;
    static const AtomicProperties Li;
    static const AtomicProperties Be;
    static const AtomicProperties B;
    static const AtomicProperties C;
    static const AtomicProperties N;
    static const AtomicProperties O;
    static const AtomicProperties F;
    static const AtomicProperties Ne;
    static const AtomicProperties Na;
    static const AtomicProperties Mg;
    static const AtomicProperties Al;
    static const AtomicProperties Si;
    static const AtomicProperties P;
    static const AtomicProperties S;
    static const AtomicProperties Cl;
    static const AtomicProperties Ar;
    static const AtomicProperties K;
    static const AtomicProperties Ca;
    static const AtomicProperties Sc;
    static const AtomicProperties Ti;
    static const AtomicProperties V;
    static const AtomicProperties Cr;
    static const AtomicProperties Mn;
    static const AtomicProperties Fe;
    static const AtomicProperties Co;
    static const AtomicProperties Ni;
    static const AtomicProperties Cu;
    static const AtomicProperties Zn;
    static const AtomicProperties Ga;
    static const AtomicProperties Ge;
    static const AtomicProperties As;
    static const AtomicProperties Se;
    static const AtomicProperties Br;
    static const AtomicProperties Kr;
    static const AtomicProperties Rb;
    static const AtomicProperties Sr;
    static const AtomicProperties Y;
    static const AtomicProperties Zr;
    static const AtomicProperties Nb;
    static const AtomicProperties Mo;
    static const AtomicProperties Tc;
    static const AtomicProperties Ru;
    static const AtomicProperties Rh;
    static const AtomicProperties Pd;
    static const AtomicProperties Ag;
    static const AtomicProperties Cd;
    static const AtomicProperties In;
    static const AtomicProperties Sn;
    static const AtomicProperties Sb;
    static const AtomicProperties Te;
    static const AtomicProperties I;
    static const AtomicProperties Xe;
    static const AtomicProperties Cs;
    static const AtomicProperties Ba;
    static const AtomicProperties La;
    static const AtomicProperties Ce;
    static const AtomicProperties Pr;
    static const AtomicProperties Nd;
    static const AtomicProperties Pm;
    static const AtomicProperties Sm;
    static const AtomicProperties Eu;
    static const AtomicProperties Gd;
    static const AtomicProperties Tb;
    static const AtomicProperties Dy;
    static const AtomicProperties Ho;
    static const AtomicProperties Er;
    static const AtomicProperties Tm;
    static const AtomicProperties Yb;
    static const AtomicProperties Lu;
    static const AtomicProperties Hf;
    static const AtomicProperties Ta;
    static const AtomicProperties W;
    static const AtomicProperties Re;
    static const AtomicProperties Os;
    static const AtomicProperties Ir;
    static const AtomicProperties Pt;
    static const AtomicProperties Au;
    static const AtomicProperties Hg;
    static const AtomicProperties Tl;
    static const AtomicProperties Pb;
    static const AtomicProperties Bi;
    static const AtomicProperties Po;
    static const AtomicProperties At;
    static const AtomicProperties Rn;
    static const AtomicProperties Fr;
    static const AtomicProperties Ra;
    static const AtomicProperties Ac;
    static const AtomicProperties Th;
    static const AtomicProperties Pa;
    static const AtomicProperties U;
    static const AtomicProperties Np;
    static const AtomicProperties Pu;
    static const AtomicProperties Am;
    static const AtomicProperties Cm;
    static const AtomicProperties Bk;
    static const AtomicProperties Cf;
    static const AtomicProperties Es;
    static const AtomicProperties Fm;
    static const AtomicProperties Md;
    static const AtomicProperties No;
    static const AtomicProperties Lr;

public:
    double getMass() const;
    int getNumber() const;
    const QString& getSymbol() const;
    double getCovalentRadius() const;

    static const AtomicProperties* fromAtomicNumber(int number);
    static const AtomicProperties* fromSymbol(const QString& symbol);
    static QVector<QString> allSymbols();
    bool operator==(const AtomicProperties& other) const;

private:
    static QMap<QString, const AtomicProperties*> mappingFromSymbol;
    static QMap<int, const AtomicProperties*> mappingFromNumber;
    int number;
    QString symbol;
    double mass;
    double covalent_radius;

private:
    AtomicProperties(int number, const QString& symbol, double mass, double covalent_radius);
};

}
#endif // ATOMICPROPERTIES_H
