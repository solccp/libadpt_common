#include "io_utils.h"

#include <QFile>
#include <QTextStream>

#include <stdexcept>

namespace ADPT
{

    void writeFile(const QString &filename, const QString &content)
    {
        QFile file(filename);
        if (file.open(QFile::WriteOnly))
        {
            QTextStream stream(&file);
            stream << content ;
            return;
        }
        throw std::runtime_error("Cannot open " + filename.toStdString() + " for writing.");
    }

}
