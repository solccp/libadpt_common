#ifndef PROCESS_UTILS_H
#define PROCESS_UTILS_H


#include <QString>

namespace ADPT
{
    QString which(const QString& exec);
    QString expandpath(const QString& file);

    bool ExecuteCommandStream(const QString& workingDir,
                              const QString& executable,
                              const QStringList& arguments,
                              QByteArray& output,
                              const QByteArray& infile, int timeout=-1);

    bool ExecuteCommand(const QString& workingDir,
                        const QString& executable,
                        const QStringList& arguments,
                        QByteArray &output,
                        const QString& infile, int timeout = -1);
}




#endif // PROCESS_UTILS_H

