#include "basic_types.h"
#include "process_utils.h"
#include "utils.h"

namespace ADPT
{
PotentialName PotentialName::fromString(const QString &potential)
{
    QStringList arr = potential.split("-");
    if (arr.size() != 2)
    {
        throw std::invalid_argument("Invaild potential name");
    }
    auto elem1 = arr[0];
    auto elem2 = arr[1];
    return PotentialName(elem1, elem2);
}

QString PotentialName::getElement1() const
{
    return element1;
}

void PotentialName::setElement1(const QString &value)
{
    element1 = value;
}

QString PotentialName::getElement2() const
{
    return element2;
}

void PotentialName::setElement2(const QString &value)
{
    element2 = value;
}

PotentialName::PotentialName(const QString &elem1, const QString &elem2)
    {
        element1 = elem1;
        element2 = elem2;
    }

    PotentialName::PotentialName(const QPair<QString, QString> &pair)
    {
        element1 = pair.first;
        element2 = pair.second;
    }

    bool PotentialName::setPotentialName(const QString &pair_str)
    {
        QStringList arr = pair_str.split("-");
        if (arr.size() != 2)
        {
            return false;
        }
        element1 = arr[0];
        element2 = arr[1];
        return true;
    }

    QString PotentialName::toString()
    {
        return QString("%1-%2").arg(element1).arg(element2);
    }

    PotentialName PotentialName::swapNames() const
    {
        return PotentialName(element2, element1);
    }

    void PotentialName::sort()
    {
        if (element1 != element2)
        {
            if (element1 > element2)
            {
                qSwap(element1, element2);
            }
        }
    }

    SKFileInfo &SKFileInfo::operator=(const SKFileInfo &rhs)
    {
        prefix    = rhs.prefix;
        separator = rhs.separator;
        suffix    = rhs.suffix;
        lowercase = rhs.lowercase;
        type2Names = rhs.type2Names;
        maxAngularMomentum = rhs.maxAngularMomentum;
        selectedShells = rhs.selectedShells;
        skfiles = rhs.skfiles;
        return *this;
    }

    QStringList SKFileInfo::getSKFileName(const QString &elem1_, const QString &elem2_) const
    {
        QStringList res;
        if (this->type2Names)
        {
            QString elem1 = elem1_;
            QString elem2 = elem2_;
            if (lowercase)
            {
                elem1 = elem1.toLower();
                elem2 = elem2.toLower();
            }

            QString filename = QString("%1%2%3%4%5").arg(prefix)
                    .arg(elem1).arg(separator).arg(elem2).arg(suffix);
            QFileInfo fileinfo(filename);
            if (fileinfo.isFile() && fileinfo.exists())
            {
                res.append(filename);
            }
        }
        else
        {
            QMapIterator<QString, QStringList> it(skfiles);
            while(it.hasNext())
            {
                it.next();
                auto poten = ADPT::PotentialName::fromString(it.key());
                if (poten.getElement1() == elem1_ && poten.getElement2() == elem2_)
                {
                    res = it.value();
                    break;
                }
            }
        }
        return res;
    }

    void SKFileInfo::makeAbsolutePath(const QDir& rootDir)
    {
        if (this->type2Names)
        {
            makeAbsolutePath_local(rootDir, prefix);
        }
        else
        {
            QMutableMapIterator <QString, QStringList> it(skfiles);
            while(it.hasNext())
            {
                it.next();
                for(auto & item : it.value())
                {
                    makeAbsolutePath_local(rootDir, item);
                }
            }
        }
    }

    void SKFileInfo::makeRelativePath(const QDir &rootDir)
    {
        if (this->type2Names)
        {
            prefix = rootDir.relativeFilePath(prefix);
        }
        else
        {
            QMutableMapIterator <QString, QStringList> it(skfiles);
            while(it.hasNext())
            {
                it.next();
                for(auto & item : it.value())
                {
                    item = rootDir.relativeFilePath(item);
                }
            }
        }
    }

    int SKFileInfo::getMaxAngularMomentum(const QString& element) const
    {
        QString index = "spdfghi";
        if (type2Names)
        {
            if (maxAngularMomentum.contains(element))
            {
                return index.indexOf(maxAngularMomentum[element]);
            }
            else
            {
                return 0;
            }
        }
        else
        {
            if (selectedShells.contains(element))
            {

                int maxL = 0;
                for (int i=0; i<selectedShells[element].size(); ++i)
                {
                    for(int j=0; j<selectedShells[element][i].size();++j)
                    {
                        if (selectedShells[element][i][j] >  maxL)
                        {
                            maxL = index.indexOf(selectedShells[element][i][j]);
                        }
                    }
                }
                return maxL;
            }
            else
            {
                return 0;
            }
        }
    }

    int KPointsSetting::numOfPoints() const
    {
        if (m_type == Type::KLines)
        {
            int size = 0;
            for(auto const & item : this->m_kpoints_klines)
            {
                size += item.num_of_points;
            }
            return size;
        }
        else if (m_type == Type::Plain)
        {
            return m_kpoints_plain.size();
        }
        else
        {
            //! TODO not good way to calcumate num of points. Even this should not be called.
            return m_kpoints_supercell.num_of_cells[0][0]*m_kpoints_supercell.num_of_cells[1][1]*m_kpoints_supercell.num_of_cells[2][2];
        }
    }

    QString UUID::toString() const
    {
        QString res = m_uuid.toString().mid(1,36);
        return res;
    }

    void UUID::fromString(const QString &str)
    {
        m_uuid = QUuid(str);
    }

    bool UUID::operator==(const UUID &rhs) const
    {
        if (this == &rhs)
        {
            return true;
        }
        return m_uuid == rhs.m_uuid;
    }

    bool UUID::operator!=(const UUID &rhs) const
    {
        return !(*this == rhs);
    }

    UUID &UUID::operator=(const QString &rhs)
    {
        fromString(rhs);
        return *this;
    }

}
