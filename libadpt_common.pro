QT       -= gui

TARGET = adpt_common
TEMPLATE = lib
CONFIG += staticlib c++11

HEADERS += \
    atomicproperties.h variantqt.h \
    process_utils.h \
    io_utils.h \
    basic_types.h \
    utils.h

SOURCES += \
    atomicproperties.cpp \
    process_utils.cpp \
    io_utils.cpp \
    basic_types.cpp \
    utils.cpp


INCLUDEPATH += $$PWD/../external_libs/include/
