#include "atomicproperties.h"

#include <QString>
#include <QVector>

namespace ADPT
{


QMap<QString, const AtomicProperties*> AtomicProperties::mappingFromSymbol;
QMap<int, const AtomicProperties*> AtomicProperties::mappingFromNumber;


const AtomicProperties AtomicProperties::ERROR_ATOM = AtomicProperties(  0,"ERROR", 0.0 ,0.00);
const AtomicProperties AtomicProperties::H     = AtomicProperties(  1, "H",  1.0079,0.23);
const AtomicProperties AtomicProperties::He    = AtomicProperties(  2,"He",  4.0026,0.28);
const AtomicProperties AtomicProperties::Li    = AtomicProperties(  3,"Li",  6.941 ,1.20);
const AtomicProperties AtomicProperties::Be    = AtomicProperties(  4,"Be",  9.0122,0.35);
const AtomicProperties AtomicProperties::B     = AtomicProperties(  5, "B", 10.811 ,0.83);
const AtomicProperties AtomicProperties::C     = AtomicProperties(  6, "C", 12.0107,0.68);
const AtomicProperties AtomicProperties::N     = AtomicProperties(  7, "N", 14.0067,0.68);
const AtomicProperties AtomicProperties::O     = AtomicProperties(  8, "O", 15.9994,0.68);
const AtomicProperties AtomicProperties::F     = AtomicProperties(  9, "F", 18.9984,0.64);
const AtomicProperties AtomicProperties::Ne    = AtomicProperties( 10,"Ne", 20.1797,0.58);
const AtomicProperties AtomicProperties::Na    = AtomicProperties( 11,"Na", 22.9898,0.97);
const AtomicProperties AtomicProperties::Mg    = AtomicProperties( 12,"Mg", 24.305 ,1.10);
const AtomicProperties AtomicProperties::Al    = AtomicProperties( 13,"Al", 26.9815,1.35);
const AtomicProperties AtomicProperties::Si    = AtomicProperties( 14,"Si", 28.0855,1.20);
const AtomicProperties AtomicProperties::P     = AtomicProperties( 15, "P", 30.9738,1.05);
const AtomicProperties AtomicProperties::S     = AtomicProperties( 16, "S", 32.065 ,1.02);
const AtomicProperties AtomicProperties::Cl    = AtomicProperties( 17,"Cl", 35.453 ,0.99);
const AtomicProperties AtomicProperties::Ar    = AtomicProperties( 18,"Ar", 39.948 ,1.51);
const AtomicProperties AtomicProperties::K     = AtomicProperties( 19, "K", 39.0983,1.33);
const AtomicProperties AtomicProperties::Ca    = AtomicProperties( 20,"Ca", 40.078 ,0.99);
const AtomicProperties AtomicProperties::Sc    = AtomicProperties( 21,"Sc", 44.9559,1.44);
const AtomicProperties AtomicProperties::Ti    = AtomicProperties( 22,"Ti", 47.867 ,1.47);
const AtomicProperties AtomicProperties::V     = AtomicProperties( 23, "V", 50.9415,1.33);
const AtomicProperties AtomicProperties::Cr    = AtomicProperties( 24,"Cr", 51.9961,1.35);
const AtomicProperties AtomicProperties::Mn    = AtomicProperties( 25,"Mn", 54.938 ,1.35);
const AtomicProperties AtomicProperties::Fe    = AtomicProperties( 26,"Fe", 55.845 ,1.34);
const AtomicProperties AtomicProperties::Co    = AtomicProperties( 27,"Co", 58.9332,1.33);
const AtomicProperties AtomicProperties::Ni    = AtomicProperties( 28,"Ni", 58.6934,1.50);
const AtomicProperties AtomicProperties::Cu    = AtomicProperties( 29,"Cu", 63.546 ,1.52);
const AtomicProperties AtomicProperties::Zn    = AtomicProperties( 30,"Zn", 65.409 ,1.45);
const AtomicProperties AtomicProperties::Ga    = AtomicProperties( 31,"Ga", 69.723 ,1.22);
const AtomicProperties AtomicProperties::Ge    = AtomicProperties( 32,"Ge", 72.64  ,1.17);
const AtomicProperties AtomicProperties::As    = AtomicProperties( 33,"As", 74.9216,1.21);
const AtomicProperties AtomicProperties::Se    = AtomicProperties( 34,"Se", 78.96  ,1.22);
const AtomicProperties AtomicProperties::Br    = AtomicProperties( 35,"Br", 79.904 ,1.21);
const AtomicProperties AtomicProperties::Kr    = AtomicProperties( 36,"Kr", 83.798 ,1.50);
const AtomicProperties AtomicProperties::Rb    = AtomicProperties( 37,"Rb", 85.4678,1.47);
const AtomicProperties AtomicProperties::Sr    = AtomicProperties( 38,"Sr", 87.62  ,1.12);
const AtomicProperties AtomicProperties::Y     = AtomicProperties( 39, "Y", 88.9059,1.78);
const AtomicProperties AtomicProperties::Zr    = AtomicProperties( 40,"Zr", 91.224 ,1.56);
const AtomicProperties AtomicProperties::Nb    = AtomicProperties( 41,"Nb", 92.9064,1.48);
const AtomicProperties AtomicProperties::Mo    = AtomicProperties( 42,"Mo", 95.94  ,1.47);
const AtomicProperties AtomicProperties::Tc    = AtomicProperties( 43,"Tc", 98.0   ,1.47);
const AtomicProperties AtomicProperties::Ru    = AtomicProperties( 44,"Ru",101.07  ,1.40);
const AtomicProperties AtomicProperties::Rh    = AtomicProperties( 45,"Rh",102.9055,1.45);
const AtomicProperties AtomicProperties::Pd    = AtomicProperties( 46,"Pd",106.42  ,1.50);
const AtomicProperties AtomicProperties::Ag    = AtomicProperties( 47,"Ag",107.8682,1.59);
const AtomicProperties AtomicProperties::Cd    = AtomicProperties( 48,"Cd",112.411 ,1.69);
const AtomicProperties AtomicProperties::In    = AtomicProperties( 49,"In",114.818 ,1.63);
const AtomicProperties AtomicProperties::Sn    = AtomicProperties( 50,"Sn",118.71  ,1.46);
const AtomicProperties AtomicProperties::Sb    = AtomicProperties( 51,"Sb",121.76  ,1.46);
const AtomicProperties AtomicProperties::Te    = AtomicProperties( 52,"Te",127.6   ,1.47);
const AtomicProperties AtomicProperties::I     = AtomicProperties( 53, "I",126.9045,1.40);
const AtomicProperties AtomicProperties::Xe    = AtomicProperties( 54,"Xe",131.293 ,1.50);
const AtomicProperties AtomicProperties::Cs    = AtomicProperties( 55,"Cs",132.9055,1.67);
const AtomicProperties AtomicProperties::Ba    = AtomicProperties( 56,"Ba",137.327 ,1.34);
const AtomicProperties AtomicProperties::La    = AtomicProperties( 57,"La",138.9055,1.87);
const AtomicProperties AtomicProperties::Ce    = AtomicProperties( 58,"Ce",140.116 ,2.04);
const AtomicProperties AtomicProperties::Pr    = AtomicProperties( 59,"Pr",140.9076,1.82);
const AtomicProperties AtomicProperties::Nd    = AtomicProperties( 60,"Nd",144.24  ,2.01);
const AtomicProperties AtomicProperties::Pm    = AtomicProperties( 61,"Pm",145.0   ,1.99);
const AtomicProperties AtomicProperties::Sm    = AtomicProperties( 62,"Sm",150.36  ,1.80);
const AtomicProperties AtomicProperties::Eu    = AtomicProperties( 63,"Eu",151.964 ,1.99);
const AtomicProperties AtomicProperties::Gd    = AtomicProperties( 64,"Gd",157.25  ,1.79);
const AtomicProperties AtomicProperties::Tb    = AtomicProperties( 65,"Tb",158.9253,1.76);
const AtomicProperties AtomicProperties::Dy    = AtomicProperties( 66,"Dy",162.5   ,1.92);
const AtomicProperties AtomicProperties::Ho    = AtomicProperties( 67,"Ho",164.9303,1.74);
const AtomicProperties AtomicProperties::Er    = AtomicProperties( 68,"Er",167.259 ,1.89);
const AtomicProperties AtomicProperties::Tm    = AtomicProperties( 69,"Tm",168.9342,1.90);
const AtomicProperties AtomicProperties::Yb    = AtomicProperties( 70,"Yb",173.04  ,1.94);
const AtomicProperties AtomicProperties::Lu    = AtomicProperties( 71,"Lu",174.967 ,1.72);
const AtomicProperties AtomicProperties::Hf    = AtomicProperties( 72,"Hf",178.49  ,1.57);
const AtomicProperties AtomicProperties::Ta    = AtomicProperties( 73,"Ta",180.9479,1.43);
const AtomicProperties AtomicProperties::W     = AtomicProperties( 74, "W",183.84  ,1.37);
const AtomicProperties AtomicProperties::Re    = AtomicProperties( 75,"Re",186.207 ,1.35);
const AtomicProperties AtomicProperties::Os    = AtomicProperties( 76,"Os",190.23  ,1.37);
const AtomicProperties AtomicProperties::Ir    = AtomicProperties( 77,"Ir",192.217 ,1.32);
const AtomicProperties AtomicProperties::Pt    = AtomicProperties( 78,"Pt",195.078 ,1.50);
const AtomicProperties AtomicProperties::Au    = AtomicProperties( 79,"Au",196.9666,1.50);
const AtomicProperties AtomicProperties::Hg    = AtomicProperties( 80,"Hg",200.59  ,1.70);
const AtomicProperties AtomicProperties::Tl    = AtomicProperties( 81,"Tl",204.3833,1.55);
const AtomicProperties AtomicProperties::Pb    = AtomicProperties( 82,"Pb",207.2   ,1.54);
const AtomicProperties AtomicProperties::Bi    = AtomicProperties( 83,"Bi",208.9804,1.48);
const AtomicProperties AtomicProperties::Po    = AtomicProperties( 84,"Po",209.0   ,1.40);
const AtomicProperties AtomicProperties::At    = AtomicProperties( 85,"At",210.0   ,1.50);
const AtomicProperties AtomicProperties::Rn    = AtomicProperties( 86,"Rn",222.0   ,1.50);
const AtomicProperties AtomicProperties::Fr    = AtomicProperties( 87,"Fr",223.0   ,2.60);
const AtomicProperties AtomicProperties::Ra    = AtomicProperties( 88,"Ra",226.0   ,2.21);
const AtomicProperties AtomicProperties::Ac    = AtomicProperties( 89,"Ac",227.0   ,2.15);
const AtomicProperties AtomicProperties::Th    = AtomicProperties( 90,"Th",232.0381,2.06);
const AtomicProperties AtomicProperties::Pa    = AtomicProperties( 91,"Pa",231.0359,2.00);
const AtomicProperties AtomicProperties::U     = AtomicProperties( 92, "U",238.0289,1.58);
const AtomicProperties AtomicProperties::Np    = AtomicProperties( 93,"Np",237.0   ,1.90);
const AtomicProperties AtomicProperties::Pu    = AtomicProperties( 94,"Pu",244.0   ,1.87);
const AtomicProperties AtomicProperties::Am    = AtomicProperties( 95,"Am",243.0   ,1.80);
const AtomicProperties AtomicProperties::Cm    = AtomicProperties( 96,"Cm",247.0   ,1.69);
const AtomicProperties AtomicProperties::Bk    = AtomicProperties( 97,"Bk",247.0   ,1.60);
const AtomicProperties AtomicProperties::Cf    = AtomicProperties( 98,"Cf",251.0   ,1.60);
const AtomicProperties AtomicProperties::Es    = AtomicProperties( 99,"Es",252.0   ,1.60);
const AtomicProperties AtomicProperties::Fm    = AtomicProperties(100,"Fm",257.0   ,1.60);
const AtomicProperties AtomicProperties::Md    = AtomicProperties(101,"Md",258.0   ,1.60);
const AtomicProperties AtomicProperties::No    = AtomicProperties(102,"No",259.0   ,1.60);
const AtomicProperties AtomicProperties::Lr    = AtomicProperties(103,"Lr",262.0   ,1.60);

const AtomicProperties* AtomicProperties::fromAtomicNumber(int number)
{
    auto it = mappingFromNumber.find(number);
    if (it != mappingFromNumber.end())
    {
        return (it.value());
    }
    else
    {
        return &AtomicProperties::ERROR_ATOM;
    }
}

const AtomicProperties* AtomicProperties::fromSymbol(const QString &symbol)
{
    auto it = mappingFromSymbol.find(symbol);
    if (it != mappingFromSymbol.end())
    {
        return (it.value());
    }
    else
    {
        return &AtomicProperties::ERROR_ATOM;
    }
}

QVector<QString> AtomicProperties::allSymbols()
{
    QVector<QString> res;
    for (auto p = mappingFromNumber.begin(); p!= mappingFromNumber.end(); ++p)
    {
        if (p.key() == 0)
            continue;
        res.append(p.value()->getSymbol());
    }
    return res;
}

bool AtomicProperties::operator ==(const AtomicProperties &other) const
{
    if (this == &other)
        return true;
    if (this->number == other.number && this->symbol == other.symbol)
        return true;
    return false;

}

AtomicProperties::AtomicProperties(int number, const QString &symbol, double mass, double covalent_radius)
    : number(number), symbol(symbol), mass(mass), covalent_radius(covalent_radius)
{
    AtomicProperties::mappingFromNumber.insert(this->number,this);
    AtomicProperties::mappingFromSymbol.insert(this->symbol,this);
    AtomicProperties::mappingFromSymbol.insert(this->symbol.toLower(),this);
}

double AtomicProperties::getMass() const
{
    return mass;
}

int AtomicProperties::getNumber() const
{
    return number;
}

const QString &AtomicProperties::getSymbol() const
{
    return symbol;
}

double AtomicProperties::getCovalentRadius() const
{
    return covalent_radius;
}

}
