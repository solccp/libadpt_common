#ifndef IO_UTILS_H
#define IO_UTILS_H

#include <QString>

namespace ADPT
{
    void writeFile(const QString &filename, const QString &content);
}


#endif // IO_UTILS_H

