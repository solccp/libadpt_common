#include "utils.h"
#include "process_utils.h"
#include <QRegExp>
namespace ADPT{


QString slugify(const QString& str)
{
    QString res = str;
    if (res.startsWith(".."))
    {
        res = res.mid(2);
    }
    else if (res.startsWith(".") || res.startsWith("/"))
    {
        res = res.mid(1);
    }


    {
        res.replace( QRegExp( "[" + QRegExp::escape( "\\/:*?\"<>|" ) + "]" ), "_");
    }

    if (res.length() > 250)
    {
        res = res.mid(0,200) + res.mid(res.size()-36);
    }
    return res;
}

void makeAbsolutePath_local(const QDir& rootDir, QString& path)
{
    if (path.isEmpty())
        return;
    QFileInfo info(path);

    if (info.isAbsolute())
    {
        return;
    }
    else
    {
        QString file = ADPT::expandpath(path);
        if (QFileInfo(file).exists())
        {
            path = file;
        }
        else
        {
            path = rootDir.absoluteFilePath(path);
        }
    }
}


}
