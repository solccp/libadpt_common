#ifndef BASIC_TYPES_H
#define BASIC_TYPES_H

#include <QString>
#include <QStringList>
#include <QMap>
#include <QDir>
#include <QUuid>

#include "variantqt.h"


namespace ADPT
{
    class PotentialName
    {
    public:
        static PotentialName fromString(const QString& potential);

    public:
        QString getElement1() const;
        void setElement1(const QString &value);
        QString getElement2() const;
        void setElement2(const QString &value);
        PotentialName() = delete;
        PotentialName(const QString& elem1, const QString& elem2);
        PotentialName(const QPair<QString, QString>& pair);
        bool setPotentialName(const QString& pair_str);
        QString toString();
        PotentialName swapNames() const;
        void sort();
    private:
        QString element1;
        QString element2;
    };


    class SKFileInfo
    {
    public:
        SKFileInfo() = default;
        SKFileInfo(const SKFileInfo& rhs) = default;
        SKFileInfo& operator=(const SKFileInfo& rhs);

        QString prefix    = "";
        QString separator = "-";
        QString suffix    = ".skf";
        bool    lowercase = false;
        bool    type2Names = true;
        QMap<QString, QString> maxAngularMomentum;
        QMap<QString, QStringList> selectedShells;

        QMap<QString, QStringList> skfiles;

        QStringList getSKFileName(const QString& elem1_, const QString& elem2_) const;
        void makeAbsolutePath(const QDir& rootDir);
        void makeRelativePath(const QDir& rootDir);
        int getMaxAngularMomentum(const QString &element) const;

    };


    class KLinesEntry
    {
    public:
        int num_of_points;
        dVector3 kpoint;
    };

    class KPointsAndWeightEntry
    {
    public:
        dVector3 kpoint;
        double weight;
    };

    class KPointShpercellFolding
    {
    public:
        iMatrix3x3 num_of_cells = {{{{0,0,0}}, {{0,0,0}}, {{0,0,0}}}};
        dVector3 shifts = {{0,0,0}};
    };


    class KPointsSetting
    {
    public:
        enum class Type : int
        {
            Plain = 0,
            SupercellFolding = 1,
            KLines = 2
        };

        Type m_type = Type::Plain;
        QList<KPointsAndWeightEntry> m_kpoints_plain;
        QList<KLinesEntry> m_kpoints_klines;
        KPointShpercellFolding m_kpoints_supercell;
        int numOfPoints() const ;
    };

    class UUID
    {
    public:
        QString toString() const;
        void fromString(const QString& str);
        bool operator==(const UUID& rhs) const;
        bool operator!=(const UUID& rhs) const;
        UUID& operator=(const QString& rhs);
    private:
        QUuid m_uuid;
    };



}

namespace libvariant
{
    template<>
    struct VariantConvertor<ADPT::SKFileInfo>
    {
        static Variant toVariant(const ADPT::SKFileInfo &rhs)
        {
            Variant var;
            var["type2filenames"] = rhs.type2Names;
            if (rhs.type2Names)
            {
                var["lowercase"] = rhs.lowercase;
                var["max_angular_momentum"] = VariantConvertor<QMap<QString, QString> >::toVariant(rhs.maxAngularMomentum);
                var["prefix"] = rhs.prefix.toStdString();
                var["separator"] = rhs.separator.toStdString();
                var["suffix"] = rhs.suffix.toStdString();
            }
            else
            {
                using mssl = QMap<QString, QStringList>;
                var["skfiles"] = VariantConvertor<mssl>::toVariant(rhs.skfiles);
                var["selected_shells"] = VariantConvertor<mssl>::toVariant(rhs.selectedShells);
            }
            return var;
        }
        static void fromVariant(const Variant &var, ADPT::SKFileInfo &rhs)
        {
            bool hasValue;
            GET_VARIANT(rhs.type2Names, var, "type2filenames", bool);
            if (rhs.type2Names)
            {
                GET_VARIANT_OPT(rhs.lowercase, var, "lowercase", bool, hasValue);
                GET_VARIANT_OPT(rhs.prefix, var, "prefix", QString, hasValue);
                GET_VARIANT_OPT(rhs.separator, var, "separator", QString, hasValue);
                GET_VARIANT_OPT(rhs.suffix, var, "suffix", QString, hasValue);
                using mss = QMap<QString, QString>;
                GET_VARIANT(rhs.maxAngularMomentum, var, "max_angular_momentum", mss );
            }
            else
            {
                using mssl = QMap<QString, QStringList>;
                GET_VARIANT_OPT(rhs.skfiles, var, "skfiles", mssl, hasValue);
                if (!rhs.skfiles.empty())
                {
                    GET_VARIANT(rhs.selectedShells, var, "selected_shells", mssl);
                }
            }
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<ADPT::KLinesEntry>
    {
        static libvariant::Variant toVariant(const ADPT::KLinesEntry &rhs)
        {
            Variant var;
            var.Append(rhs.num_of_points);
            var.Append(rhs.kpoint[0]);
            var.Append(rhs.kpoint[1]);
            var.Append(rhs.kpoint[2]);
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ADPT::KLinesEntry &rhs)
        {
            bool hasValue;
            GET_VARIANT_LIST(rhs.num_of_points, var, 0, int);
            GET_VARIANT_LIST(rhs.kpoint[0], var, 1, double);
            GET_VARIANT_LIST(rhs.kpoint[1], var, 2, double);
            GET_VARIANT_LIST(rhs.kpoint[2], var, 3, double);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<ADPT::KPointShpercellFolding>
    {
        static libvariant::Variant toVariant(const ADPT::KPointShpercellFolding &rhs)
        {
            Variant var;
            for(auto i: {0,1,2})
            {
                var.Append(VariantConvertor<iVector3>::toVariant(rhs.num_of_cells[i]));
            }
            var.Append(VariantConvertor<dVector3>::toVariant(rhs.shifts));
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ADPT::KPointShpercellFolding &rhs)
        {
            bool hasValue;
            GET_VARIANT_LIST(rhs.num_of_cells[0], var, 0, iVector3);
            GET_VARIANT_LIST(rhs.num_of_cells[1], var, 1, iVector3);
            GET_VARIANT_LIST(rhs.num_of_cells[2], var, 2, iVector3);
            GET_VARIANT_LIST(rhs.shifts, var, 3, dVector3);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<ADPT::KPointsAndWeightEntry>
    {
        static libvariant::Variant toVariant(const ADPT::KPointsAndWeightEntry &rhs)
        {
            Variant var;
            var.Append(rhs.kpoint[0]);
            var.Append(rhs.kpoint[1]);
            var.Append(rhs.kpoint[2]);
            var.Append(rhs.weight);
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ADPT::KPointsAndWeightEntry &rhs)
        {
            bool hasValue;
            GET_VARIANT_LIST(rhs.kpoint[0], var, 0, double);
            GET_VARIANT_LIST(rhs.kpoint[1], var, 1, double);
            GET_VARIANT_LIST(rhs.kpoint[2], var, 2, double);
            GET_VARIANT_LIST(rhs.weight, var, 3, double);
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<ADPT::KPointsSetting>
    {
        static libvariant::Variant toVariant(const ADPT::KPointsSetting &rhs)
        {
            Variant var;

            if (rhs.m_type == ADPT::KPointsSetting::Type::Plain)
            {
                var["plain"] = VariantConvertor<QList<ADPT::KPointsAndWeightEntry> >::toVariant(rhs.m_kpoints_plain);
            }
            else if (rhs.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
            {
                var["supercell_folding"] = VariantConvertor<ADPT::KPointShpercellFolding>::toVariant(rhs.m_kpoints_supercell);
            }
            else if (rhs.m_type == ADPT::KPointsSetting::Type::KLines)
            {
                var["klines"] = VariantConvertor<QList<ADPT::KLinesEntry>>::toVariant(rhs.m_kpoints_klines);
            }
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ADPT::KPointsSetting &rhs)
        {
            bool hasValue;

            if (var.Contains("plain"))
            {
                GET_VARIANT(rhs.m_kpoints_plain, var, "plain", QList<ADPT::KPointsAndWeightEntry>);
                rhs.m_type = ADPT::KPointsSetting::Type::Plain;
            }
            else if (var.Contains("supercell_folding"))
            {
                GET_VARIANT(rhs.m_kpoints_supercell, var, "supercell_folding", ADPT::KPointShpercellFolding);
                rhs.m_type = ADPT::KPointsSetting::Type::SupercellFolding;
            }
            else if (var.Contains("klines"))
            {
                GET_VARIANT(rhs.m_kpoints_klines, var, "klines", QList<ADPT::KLinesEntry>);
                rhs.m_type = ADPT::KPointsSetting::Type::KLines;
            }

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<ADPT::UUID>
    {
        static Variant toVariant(const ADPT::UUID &rhs)
        {
            Variant var = rhs.toString().toStdString();
            return var;
        }
        static void fromVariant(const Variant &var, ADPT::UUID &rhs)
        {
            rhs.fromString(QString::fromStdString(var.AsString()));
        }
    };

}


#endif // BASIC_TYPES_H

